# Django Rest Framework Demo

Uses Poetry, Django and Rest Framework to create a very simple REST API web application. Uses LV2020 for client side. 
Implements GRUD operations.

It contains only one model **experiments** containing textfields **name** and **description**. You can get table of experiments
with http://server:8000/experiments and use generic rest api with http://server:8000/rest_api/experiments/

The serializer derives from **rest_framework.serializers.ModelSerializer**.

The CRUD operations are obtained in **rest_api/experiments/views.py** by **inheriting from rest_framework.viewsets.ModelViewSet**


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.gsi.de/a.kessler/django-rest-framework-demo.git
git branch -M main
git push -uf origin main
```

- [ ] Install packages with:
```
poetry install
python3 manage.py runserver 0:8000
```
## LabVIEW Part
- Open Project and DijRFM Dermo.vi
- adapt URL 
- play around
- You can select an experiment by changing Id in **Add/Update Experiment** control.
