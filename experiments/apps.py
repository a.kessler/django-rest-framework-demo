from django.apps import AppConfig


class ExperimentConfig(AppConfig):
    # default_auto_field = 'django.db.models.BigAutoField'
    name = 'experiments'
    verbose_name = 'Demo for REST API'
