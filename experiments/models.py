from django.db import models


# Create your models here.
class Experiments(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(default=None, blank=True, null=True)

    # @property
    def __str__(self):
        return self.name + " ; " + self.description