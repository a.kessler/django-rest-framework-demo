from django.shortcuts import render
from .models import Experiments

# Create your views here.
from django.http import HttpResponse


def index(request):
    exps = Experiments.objects.all()
    context = {'exps': exps, }
    return render(request, 'experiments/index.html', context)


def details(request, experiment_id):
    return HttpResponse("Experment ID: %s." % experiment_id)
