from rest_framework import serializers
from experiments.models import Experiments

class ExperimentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experiments
        fields = ['id', 'name', 'description']
