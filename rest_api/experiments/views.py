from rest_framework import generics as g
from rest_framework import viewsets
from experiments.models import Experiments
from rest_framework.permissions import AllowAny
from . import serializer

class ExperimentViewSet(viewsets.ModelViewSet):
    queryset = Experiments.objects.all()
    serializer_class = serializer.ExperimentSerializer
    permission_classes = (AllowAny, )


   # def put(self):

