# a.kessler@hi-jena.gsi.de  CC-BY-4.0
# Created 12.01.2023
#
# Aufruf vom cmd Terminal: powershell -File "C:\...\LV Demo\http-patch.ps1"  -ReqURL http://polarisserver.ioq.uni-jena.de:9080/api/v3/Proposals/LV-Test_6?access_token=... -JSONString "{\"lastname\": \"Kotelkov\"}" 
# Problem: PowerShell Terminal poppt kurz auf... Suchen nach Lösung.

param($ReqURL, $JSONString)
#Write-Host $ReqURL # for debug
#Write-Host "JSONString= " $JSONString # for debug
Invoke-RestMethod  -Uri $ReqURL -Method Patch -ContentType "application/json" -Body $JSONString